Brief Documentation for ``pyccalc``
===================================

Unimaginatively named Python interface into CCalc C+ Action Language Interpreter. 

For more on CCalc, start at http://www.cs.utexas.edu/~tag/cc/

If you use this in any significant part of your project, do please cite the paper that it was built for:

Duff, D. J., Erdem, E., & Patoglu, V. (2013). Integration of 3D Object Recognition and Planning for Robotic Manipulation: A Preliminary Report. Presented at the ICLP-KRR 2013, Istanbul, Turkey. Retrieved from [http://arxiv-web3.library.cornell.edu/abs/1307.7466](http://arxiv-web3.library.cornell.edu/abs/1307.7466)

A summary of that work is here: [http://djduff.net/perceiving-while-planning/](http://djduff.net/perceiving-while-planning/)


Usage
-----

See the test script ``simpler_test`` and the associated ccalc files ``bw_2.ccalc`` (domain) and ``bw_2_prob.ccalc`` (problem) for a simple example. The approach is to set up the paths to CCalc, choose a solver, then instansiate a CCalc instance as a ``CCalcInteractor`` object. To this instance you pass ``.pl`` or ``.ccalc`` files to compile or ground and complete. Then you call ``.get_plans()`` on the ``CCalcInteractor`` object which returns an iterator that you can loop over. Note that the buffer size is not huge, so the planner will wait on the calling program before returning them (it might look ahead a bit).

For a more sophisticated example, showing the capabilities with respect to interruption and restart (you can interrupt and restart the planner, even reground it, but continue to use the same iterator), see the test script ``test_bw_2.py`` and the associated ccalc files in ``example_files``.

You should be able to get and test it with the following:

    git clone https://damienjadeduff@bitbucket.org/damienjadeduff/pyccalc.git
    cd pyccalc
    ./test_bw_2.py
    
Notes
-----

If it freezes, check the log file (you pass the filename when you create the interactor object); ccalc may have frozen on some bad input.

If while using these modules, your program drops into a debugger, it most likely means the program has encountered unexpected behaviour and entered a state from which it has no idea how to proceed. You can submit an issue attaching all the log files, input files, program files, and the backtrace.

Set ccalc location with ``ccalc_interactor.ccalc_loc="/path/to/ccalc.pl"``

Set the path to prolog with ``ccalc_interactor.swipl_loc="swipl"`` (only tested with SWI prolog).

License
-------

See the ``GPL.txt`` file.

With Compliments
----------------

I hope it is useful to you.

Disclaimer
==========

Please do not use this software if you are good. If you are good, it will try and eat your soul. Only use this software if you are slightly unhinged. Do not blame me if this software tries to eat your soul.