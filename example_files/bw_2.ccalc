% kate: indent-mode Prolog; scheme Prolog; hl Prolog
% Simplified blocks-world variant.
% Damien Jade Duff. GPLv3.

:- sorts
  placeable>>obj;
  place>>obj;
  orientation.

:- objects

  loc1,loc2 ::place;
  bolt,nut1,nut2 ::obj;
  vert,horiz_x,horiz_y :: orientation.

:- constants
  is_at(obj)                                    :: inertialFluent(place);
  orientation_is(obj)                           :: inertialFluent(orientation);
  is_below(place,obj)                           :: sdFluent;
  move(placeable,place,orientation)             :: exogenousAction.

:- variables
   PUT_ORIENTATION      :: orientation;
  OBJ,OBJ2                         :: obj;
  DEST,LOC                           :: place.
  
caused false if is_at(OBJ)=DEST,is_at(OBJ2)=DEST,OBJ<>OBJ2. %This rule can be relaxed once we use the motion planner as actually multiple objects may be stacked in one location if the stacker can find a way of doing it

% %%%%%%%% % Can't move an object to somewhere that some other object is.
nonexecutable move(OBJ,DEST,PUT_ORIENTATION) if is_at(OBJ2)=DEST. %Again, this may be possible in the future and it's up to external predicates to decide this.
% %%%%%%%% % Can't move an object to somewhere if some other object is on it.
nonexecutable move(OBJ,DEST,PUT_ORIENTATION) if is_at(OBJ2)=OBJ. %Again, this may be possible in the future and it's up to external predicates to decide this.

% %%%%%%%% % Define is_below to define relationships with table points
% %%%%%%%% % don't allow loops.
caused false if is_below(OBJ,OBJ).
caused is_below(LOC,OBJ2) if is_at(OBJ2)=LOC.   
caused is_below(LOC,OBJ2) if is_at(OBJ)=LOC & is_below(OBJ,OBJ2).
default -is_below(LOC,OBJ).

%%%%%%%% % move an object
move(OBJ,DEST,PUT_ORIENTATION) causes is_at(OBJ)=DEST.
move(OBJ,DEST,PUT_ORIENTATION) causes orientation_is(OBJ)=PUT_ORIENTATION.

%%%%%%%%% there is no point moving an object to where it is now:
nonexecutable move(OBJ,DEST,PUT_ORIENTATION) if is_at(OBJ)=DEST. % This may be possible to relax to do minor adjustments of object possible

noconcurrency.