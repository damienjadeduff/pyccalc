#!/usr/bin/env python

'''     Example file for pyccalc usage.
        
        --------------------------------------------------------------------
        
        Copyright (C) 2013  Damien Jade Duff (Delfina's Dad)

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <http://www.gnu.org/licenses/>.
            
'''   

# while 1 do; ./test_bw_2.py || exit;done # stress test

import ccalc_parser
import ccalc_interactor
import random
import os

verbosity_level=0
log_to_filename="bw_2_log.txt" #if you get in trouble, see here to see what CCalc's output was.
ccalc_example_filename="example_files/bw_2_prob1.ccalc"
solver="grasp"

if __name__ == '__main__':
  
  print "Welcome to a test script for testing modules ccalc_interactor and ccalc_parser"
  
  '''Set up the paths (change this for your system)'''
  ccalc_interactor.swipl_loc="swipl"#"pl"
  ccalc_interactor.ccalc_loc="/opt/software/ccalc/ccalc.pl"#"~/software/react/ccalc/ccalcP.pl"

  ccalc_interactor.default_solver=solver
  
  print "Let's get swipl/ccalc running, and geta  prompt..."
  ccalc=ccalc_interactor.CCalcInteractor(logfile_name=log_to_filename,num_to_ask=5,verbose=verbosity_level,query_label=2) # 5 is number of plans to get 
  
  print "Now let's load and ground the example plan..."
  ccalc.load_and_ground(ccalc_example_filename)
  
  print "Create the plan generator object..." #by which I mean a "python generator"
  plan_generator=ccalc.get_plans()
  
  plans_found=0
  num_restarts=0
  
  restartOnLast=random.random()>0.5 # for different corner cases       
  
  print "Now loop through plans as returned by the generator..."
  for raw_plan in plan_generator:
    print "Now parsing a returned plan..."
    plan=ccalc_parser.parse_plans(raw_plan)[0] #there should only be one plan to parse, so get first in list
    plans_found=plans_found+1
    print "***"
    print "We have a plan, of length",len(plan.actions),"with",len(plan.states),"timepoints."
    print "Final state is:"
    try:
      for pred in plan.states[-1]:
        print "  ",pred
    except:
      import pdb;pdb.set_trace()
    
    if (not restartOnLast and random.random()>1.0-1.0/plans_found) or (restartOnLast and plans_found==5 and num_restarts<1):
    
      print "Aborting planning."
      ccalc.abort_planning()
      #could change plan around here to do dynamic replanning
      if random.random()>0.9:
        ccalc.set_num_plans_to_ask_for(random.randint(1,10))
      if random.random()>0.3:
        print "restarting planning"
        if random.random()>0.5:
          ccalc.respawn() # gets a new process
        ccalc.load_and_ground(ccalc_example_filename) # will make plan_generator spew forth more plans
        num_restarts=num_restarts+1
      if random.random()>0.9:
        ccalc.set_num_plans_to_ask_for(random.randint(1,10))
      
      # can also use ccalc.load_pl(extern_pred_or_problem_file)
      # can also use ccalc.set_query_label(label)
      # can also use ccalc.set_sat_solver(solver)
      # can also use ccalc.set_ccalc_logfile(newlogfile)
      
    
  ccalc.close()
  
  print "Done.\n\n\n -- got"  ,plans_found,"plans, with",num_restarts,"restarts."
  
  #with open(log_to_filename,'r') as log_file:
    #print "CCALC log is:\n\n","---- ".join(log_file.readlines())
  print "See the CCalc log in",log_to_filename
    
    