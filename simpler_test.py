#!/usr/bin/env python

'''     Example file for pyccalc usage (a simpler example).
        
        --------------------------------------------------------------------
        
        Copyright (C) 2013  Damien Jade Duff (Delfina's Dad)

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <http://www.gnu.org/licenses/>.
            
'''   
import ccalc_parser
import ccalc_interactor

'''Setup CCalc'''
ccalc_interactor.swipl_loc="swipl"
ccalc_interactor.ccalc_loc="/opt/software/ccalc/ccalc.pl"
ccalc_interactor.default_solver="grasp"

'''Instantiate an interpreter'''
ccalc=ccalc_interactor.CCalcInteractor(logfile_name="simple_ccalc_test_log.txt",num_to_ask=10,verbose=0)
  
'''Load the domain'''
ccalc.load_and_ground("example_files/bw_2.ccalc")

'''Load the problem'''
ccalc.load_pl("example_files/bw_2_prob.ccalc")

'''Create the generator which will generate the plans'''
plan_generator=ccalc.get_plans()
cntr=0

'''Loop through the plans.'''
for raw_plan in plan_generator:
    '''Parse the plan into a list of states and actions'''
    plan=ccalc_parser.parse_plans(raw_plan)[0]
    '''Print out the plan length and the plan itself'''
    print "Plan #",cntr,"Length",len(plan.actions),"\n",plan
    '''Show that we can quit halfway through'''
    if cntr>5:
      ccalc.abort_planning()
    cntr=cntr+1
    
print "Finished after",cntr,"plans."