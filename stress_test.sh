#!/bin/bash
# the arithmetic below is bash specific #!/bin/sh 

#SLEEP_AMT=3
SLEEP_AMT=0

# while echo ;do echo cont |python -m pdb ./test_bw_2.py ;sleep $SLEEP_AMT;done
#while echo ;do python ./test_bw_2.py || exit ;sleep $SLEEP_AMT;echo cont |python -m pdb ./test_bw_2.py || exit ;sleep $SLEEP_AMT;done
#while echo ;do python ./test_bw_2.py || exit ; sleep $SLEEP_AMT ;done
while echo 
do 
echo ---
# can't set negative niceness without being root NICENESS=$(( $RANDOM % 40 - 20 )) # let's run it at different priorities for extra fun mix
NICENESS=$(( $RANDOM % 21 )) # let's run it at different priorities for extra fun mix
# echo NICENESS $NICENESS
echo nice -n $NICENESS python ./test_bw_2.py
nice -n $NICENESS python ./test_bw_2.py || exit 
echo ---
sleep $SLEEP_AMT 
done
