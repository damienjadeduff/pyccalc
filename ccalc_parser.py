#!/usr/bin/env python

'''     pyccalc - 
        Unimaginatively named Python interface into CCalc C+ Action Language Interpreter. 
        For more on CCalc, start at http://www.cs.utexas.edu/~tag/cc/
        
        --------------------------------------------------------------------
        
        Copyright (C) 2013  Damien Jade Duff (Delfina's Dad)

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <http://www.gnu.org/licenses/>.
            
'''

import sys  
import re
    
plan_no_exp="^([0-9]+):  "
plan_no_re=re.compile(plan_no_exp) 
  
preds_exp="\w+\(.*?\)=?\w*"
preds_re=re.compile(preds_exp)

#pred_exp="(\w+)\((\w*,)?(\w*,)?(\w*,)?(\w*,)?(\w*,)?(\w*)\)=?(\w*)"
#pred_exp="(\w+)\((\w*,\s?)?(\w*,\s?)?(\w*,\s?)?(\w*,\s?)?(\w*,\s?)?(\w*)\)\s?=?\s?(\w*)"
pred_exp="(\w+)\((\w*,\s?)?(\w*,\s?)?(\w*,\s?)?(\w*,\s?)?(\w*,\s?)?(\w*)\)\s?=?\s?(\w*)"
pred_exp_num_args_searched=6

pred_re=re.compile(pred_exp)

'''     Predicate class, holds a predicate/function - e.g. mypred(myarg1,myarg2)=result
        Initialise with a string with a predicate (single arg) or
        Initialise with args: predicate_name,[arg1,arg2,...],func_result - func result optional, arg list can be empty
'''
class Predicate:

  def __init__(self,*args):

    if len(args)==1:
      self.init_parse(args[0])
    elif len(args)==2 or len(args)==3:
      self.init_cooked(*args)
    else:
      import pdb;pdb.set_trace()

  def init_parse(self,pred_str):
    
    res=pred_re.match(pred_str)
    
    gr=res.groups()
    self.name=gr[0]
    self.args=[]
    
    for parg in gr[1:pred_exp_num_args_searched+1]:
      if parg:
        #if parg[-1]==",":
          #self.args.append(parg[0:-1])
        #else:
          #self.args.append(parg)
        self.args.append(parg.strip(' ,'))
          
    self.func_result=gr[pred_exp_num_args_searched+1]

  def init_cooked(self,name,args,func_result=None):
    
    self.name=name
    if args is not None:
      self.args=args
    else:
      self.args=[]
      
    self.func_result=func_result

  def __repr__(self):
    
    st=self.name
    
    if len(self.args)>0:
      st=st+"("

      for cntr in range(0,len(self.args)-1):
        st=st+self.args[cntr]+","
        
      st=st+self.args[len(self.args)-1]
      st=st+")"
      
    if self.func_result:
      st=st+"="+self.func_result
      
    return st
    
  def __eq__(self,other):
    return isinstance(other, self.__class__) and self.__dict__ == other.__dict__
  
  def __ne__(self,other):
    return not self.__eq__(other)
    
  def __hash__(self):
    return hash(str(self))
    
'''     Parse a whitespace-delimited string of predicates into a list of predicate objects '''
def parse_predicate_list(line):
  
    res=preds_re.findall(line)
    
    pred_list=[]
    
    for pred in res:
      this_pred=Predicate(pred)
      pred_list.append(this_pred)
    return pred_list
    
'''     A plan consists of a list of lists of states, and a list of lists of actions. 
        The list of lists of actions is 1 shorter than the list of list of states.
        States and actions are Predicate objects.
'''
class Plan:
  
  def __init__(self):
    
    self.states=[]
    self.actions=[]

  def __repr__(self):
    
    cntr=0
    st="PLAN:\n"
    
    while cntr<len(self.states):
      st=st+"  "+str(cntr)+": "+str(self.states[cntr])+"\n"
      if cntr<len(self.actions):
        st=st+"  ACTIONS: "+str(self.actions[cntr])+"\n\n"
      cntr=cntr+1
      
    return st

  ''' Get all state predicates at a certain timepoint with a certain name, and optionally matching arguments '''
  def get_state_preds(self,tp,predname,arg1=None,arg2=None,arg3=None):
    
    retpreds=[]
    
    for pred in self.states[tp]:
      if pred.name==predname:
        if arg1 is None or pred.args[0] in arg1:
          if arg2 is None or pred.args[1] in arg2:
            if arg3 is None or pred.args[2] in arg3:
              retpreds.append(pred)
              
    return retpreds

  ''' Get all action predicates at a certain timepoint with a certain name, and optionally matching arguments '''  
  def get_state_funcs(self,tp,predname,arg1=None,arg2=None,res=None):
    
    retpreds=[]
    
    for pred in self.states[tp]:
      if pred.name==predname:
        if arg1 is None or pred.args[0] in arg1:
          if arg2 is None or pred.args[1] in arg2:
            if res is None or pred.func_result in res:    
              retpreds.append(pred)
              
    return retpreds
  
  def get_pred(self,tp,**kwargs):
    retpreds=self.get_preds(tp,**kwargs)
    if len(retpreds)>1:
      import pdb;pdb.set_trace()
    elif len(retpreds)==0:
      return None
    else:
      return retpreds[0]
  
  def get_preds(self,tp,**kwargs):
    retpreds=[]
    search_predname=kwargs.get("name",None)
    search_predargs=kwargs.get("args",[])
    search_predfret=kwargs.get("ret",None)
    for pred in self.states[tp]:
      match=True
      if search_predname<>None and search_predname<>pred.name:
        match=False
        continue
      if len(search_predargs)>len(pred.args):
        match=False
        continue
      if search_predfret<>None and search_predfret<> pred.func_result:
        match=False
        continue
      for cntr in range(0,len(search_predargs)):
        if search_predargs[cntr]<>None and search_predargs[cntr]<>pred.args[cntr]:
          match=False
          continue
      if match:
        retpreds.append(pred)
    return retpreds
    

  
  def get_action(self,tp,**kwargs):
    retpreds=self.get_actions(tp,**kwargs)
    if len(retpreds)>1:
      import pdb;pdb.set_trace()
    elif len(retpreds)==0:
      return None
    else:
      return retpreds[0]    
    
  def get_actions(self,tp,**kwargs):
    retpreds=[]
    search_predname=kwargs.get("name",None)
    search_predargs=kwargs.get("args",[])
    search_predfret=kwargs.get("ret",None)
    for pred in self.actions[tp]:
      match=True
      if search_predname<>None and search_predname<>pred.name:
        match=False
        continue
      if len(search_predargs)>len(pred.args):
        match=False
        continue
      if search_predfret<>None and search_predfret<> pred.func_result:
        match=False
        continue
      for cntr in range(0,len(search_predargs)):
        if search_predargs[cntr]<>None and search_predargs[cntr]<>pred.args[cntr]:
          match=False
          continue
      if match:
        retpreds.append(pred)
    return retpreds
    
    
  #''' Application specific... shouldn't be here... '''
  #def pre_act_ori(self,stepno,source_obj):
    
    #ori_preds=self.get_state_funcs(stepno,"orientation_is",source_obj)
    #if len(ori_preds) <> 1: import pdb;pdb.set_trace()
    #source_ori=ori_preds[0].func_result 
    #return source_ori
  
  #''' Application specific... shouldn't be here... '''
  #def pre_act_table_loc(self,stepno,located_obj,okay_locs):
    
    #if located_obj in okay_locs: return located_obj
    #source_preds=self.get_state_preds(stepno,"is_below")
    
    #for pred in source_preds:
      #if pred.args[1] == located_obj:
        #if pred.args[0] in okay_locs:
          #source_loc=pred.args[0]
          
    #if source_loc is None: import pdb;pdb.set_trace()
    #return source_loc  
        

'''     Given a raw string containing just sets of plans from CCalc, parse them into a list of Plan objects. '''
def parse_plans(plansfull):
  
  started=False

  curr_plan=Plan()
  plans = []
  state=0
  lines=plansfull.splitlines()
  line_cntr=-1

  while True:
    
    line_cntr=line_cntr+1
    if line_cntr==len(lines):
      plans.append(curr_plan)
      return plans
    line=lines[line_cntr]      

    if state==0:
      planno_str=plan_no_re.match(line)
      
      if planno_str:
        
        if line[0:4]=="0:  ":
          state=1

          line_cntr=line_cntr-1
          
          if started:
            plans.append(curr_plan)
            curr_plan=Plan()
          else:
            started=True
          
          continue
        
        else:
          
          state=1
          line_cntr=line_cntr-1

          
      else:

        if line[0:8]=="ACTIONS:" :
          
          assert(planstep==len(curr_plan.actions))        
          assert(planstep==len(curr_plan.states)-1)        

          pred_list=parse_predicate_list(line)
          curr_plan.actions.append(pred_list)  
          state=0
          #continue
        
        continue
      
    elif state==1:

      planno_str=plan_no_re.match(line)
      
      if planno_str:
        
        while len(curr_plan.actions)<len(curr_plan.states):
          curr_plan.actions.append([]) #ensure we keep actions size as big as states because sometimes there are no actions at a timestep
          
        planstep=int(planno_str.group(1))
        
        
        assert(planstep==len(curr_plan.states))
        assert(planstep==len(curr_plan.actions))
        

        pred_list=parse_predicate_list(line)
        
        curr_plan.states.append(pred_list)
       
        state=0
        continue


    #elif state==2:
      #elif line[0:8]=="ACTIONS:" :
        
        #assert(planstep==len(curr_plan.actions))        
        #assert(planstep==len(curr_plan.states)-1)        

        #pred_list=parse_predicate_list(line)
        #curr_plan.actions.append(pred_list)  
        #state=0
        #continue
    else:
      print >> sys.stderr,"[parse_plans]: dodgy stuff - parser in unrecognized state (why didn't we just use a proper parser?)",state
      return plans


'''     Given a raw string containing just sets of plans from CCalc, parse them to find how many plans 
        (just looks for the number of strings like "0: ". '''
def num_plans(planfull):
  
  ind=0
  num_plans_found=0
  
  while ind < len( planfull ):
    ind = planfull.find( '0:  ', ind )
    if ind == -1:
      break
    else:
      num_plans_found=num_plans_found+1
      ind = ind + 8  
      
  return num_plans_found