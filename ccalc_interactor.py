#!/usr/bin/env python

'''     pyccalc - 
        Unimaginatively named Python interface into CCalc C+ Action Language Interpreter. 
        For more on CCalc, start at http://www.cs.utexas.edu/~tag/cc/
        NOT REENTRANT (please access this object sequentially).
        
        --------------------------------------------------------------------
        
        Copyright (C) 2013  Damien Jade Duff (Delfina's Dad)

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <http://www.gnu.org/licenses/>.
            
'''        

import re
import pexpect
import signal
import string
import os
import sys
import ccalc_parser
import inspect # for providing convenient warning messages
import time # for sleeping sometimes

ccalc_buffersize=512 # The number of characters to keep as a buffer when communicating as a terminal to CCalc.

swipl_loc="pl" #Should be parametrised
#swipl_loc="/usr/bin/swipl" #Should be parametrised
ccalc_loc="~/software/react/ccalc/ccalcP.pl" #Should be parametrised.
#ccalc_loc="/opt/software/ccalc/ccalc.pl" 
timeout=1000
'''Some solvers cannot return more than 1 solution.
GRASP and MCHAFF can (but they freeze on my system).
MINISAT, SATZ, SATZ-RAND and ZCHAFF cannot return more than one plan.
RELSAT is not compiled correctly on my system.
SATO does not return > 1 solution with full reliability.
WALKSAT is approximate.'''
default_solver="mchaff"


'''
  Encapsulates a query to be sent to CCalc. 
'''
class CCalcQuery:
  def __init__(self):
    pass
  

'''
  Encapsulates a running ccalc process.
  Before this class is used, you might want to set the variables ccalc_loc and swipl_loc to point the objects at the correct versions of swipl and ccalc.
'''
class CCalcInteractor:
  
  #def __init__(self,ccalcfile_name,logfile_name,num_to_ask,verbose=1,**kwargs):
  def __init__(self,**kwargs):
    
    self.ccalc=None
    self.logfile=None
    
    self.verbose=kwargs.get('verbose',0)
    
    self.load_ccalc=kwargs.get('load_ccalc',True)
    
    self.grounding_done=False
    self.plan_generator_active=False
    self.planning_aborted=False
    self.planning_loop_finished=False
    #self.loading_string="" # Used for returning the string that ccalc produces during loading/grounding
    self.bad_file_error=None
    
    self.set_query_label(kwargs.get('query_label',0))
    
    if self.load_ccalc:
      self.launch_com= swipl_loc+' -f '+os.path.expanduser(kwargs.get("ccalc_loc",ccalc_loc))
    else:
      self.launch_com= swipl_loc
    if self.verbose>0: print "CCALC_INTERACTOR: CCALC LAUNCH COMMAND:",self.launch_com
    
    #logfile_name
    
    
    self.respawn(kwargs.get('logfile_name',None))
    #self.set_ccalc_logfile(logfile_name) # must be spawned before this
    
    if self.load_ccalc:
      self.set_sat_solver(default_solver)
    if self.load_ccalc:
      self.set_num_plans_to_ask_for(kwargs.get('num_to_ask',1))
    
    
  def set_query_label(self,label,rerun_query=False):
    if self.plan_generator_active and not self.planning_loop_finished:
      if self.verbose>0:print "CCALC_INTERACTOR: Change to query label will only be active next time a plan is requested from CCalc."    
    self.query_label=label
    self.query_statements=[]
    #self.query_com='query '+str(self.query_label)+'.\n'
    self.query_com='query '+str(self.query_label)+'.'
    #if self.verbose>0: print "CCALC_INTERACTOR: CCALC QUERY PROBLEM COMMAND:",self.query_com[:-1]
    if self.verbose>0: print "CCALC_INTERACTOR: CCALC QUERY PROBLEM COMMAND:",self.query_com
    if rerun_query:
      self.continue_producing_plans()
    
  '''A string of statements'''
  def set_query_statements(self,query_statements,rerun_query=False):
    if self.plan_generator_active and not self.planning_loop_finished:
      if self.verbose>0:print "CCALC_INTERACTOR: Change to query string will only be active next time a plan is requested from CCalc."    
    self.query_label=""
    self.query_statements=query_statements
    #self.query_com='query '+str(self.query_label)+'.\n'
    self.query_com=""
    #if self.verbose>0: print "CCALC_INTERACTOR: CCALC QUERY PROBLEM COMMAND:",self.query_com[:-1]
    if self.verbose>0: print "CCALC_INTERACTOR: CCALC QUERY PROBLEM STRING:",self.query_statements
    if rerun_query:
      self.continue_producing_plans()
    
  '''Any old logfile should (generally) get collected (if there aren't any other references to it for e.g. a stacktrace) when all references get gone.'''
  def set_ccalc_logfile(self,logfile_name):
    self.logfile_name=logfile_name
    self.logfile=open(self.logfile_name,"w")    
    if self.ccalc is not None:
      self.ccalc.logfile=self.logfile
      
    if self.verbose>0: print "CCALC_INTERACTOR: CCALC LOG FILE:",self.logfile_name
    
  
  def respawn(self,logfile_name=None):
    if self.plan_generator_active and not self.planning_loop_finished:
      if not self.planning_aborted: # if its aborted, ccalc will be in a state that I can use
        print >> sys.stderr, "You seem to be doing things out of order"
        import pdb;pdb.set_trace()    
    
    if self.ccalc is not None:
      self.close()
      if self.verbose>1: print "CCALC_INTERACTOR: -- WARNING: if you have set a solver and a number of plans to get, you have lost that information now. Sorry for statefulness."
    if logfile_name is not None:
      self.set_ccalc_logfile(logfile_name)
    self.ccalc=pexpect.spawn(self.launch_com,timeout=timeout,maxread=ccalc_buffersize,logfile=self.logfile) #Get the process going
    self.ccalc.expect('\n\?\-')    # Wait for first prompt
    if self.verbose>1: print "CCALC_INTERACTOR: **********"
    if self.verbose>1: print "CCALC_INTERACTOR:",self.ccalc.before
    if self.verbose>1: print "CCALC_INTERACTOR: **********"
    if self.verbose>1: print "CCALC_INTERACTOR:",self.ccalc.after
    
    
      
  
  def set_sat_solver(self,solver):
    
    if self.plan_generator_active and not self.planning_loop_finished:
      if not self.planning_aborted: # if its aborted, ccalc will be in a state that I can use
        print >> sys.stderr, "You seem to be doing things out of order"
        import pdb;pdb.set_trace()    
    
      
    self.sat_solver=solver
    setsolverstring="set(solver,"+self.sat_solver+").\n\n"
    self.ccalc.sendline(setsolverstring) 
    self.ccalc.expect('\n\?\-')
    
  
  '''IMPORTANT: set the solver before the num otherwise it will probably not work (unless num happens to be 1)'''  
  def set_num_plans_to_ask_for(self,num):
    if self.plan_generator_active and not self.planning_loop_finished:
      if not self.planning_aborted: # if its aborted, ccalc will be in a state that I can use
        print >> sys.stderr, "You seem to be doing things out of order"
        import pdb;pdb.set_trace()    
    
    
    self.num_to_ask=num
    setnumstring="set(num,"+str(self.num_to_ask)+").\n\n"    
    self.ccalc.sendline(setnumstring) # Number of plans desired is set here.
    self.ccalc.expect('\n\?\-')
    
  
  
  
  def is_planner_exhausted(self):
    return self.planning_loop_finished
    
  # not necessary. reloading unsets planning_loop_finished, as it should.
  #def set_planner_renewed(self):
    #if not self.planning_loop_finished:
      #import pdb;pdb.set_trace()
    #self.planning_loop_finished=False
  
  #def reload_and_ground(self,ccalcfile_name,stats=None):
    ####all these checks happen in load_and_ground() no point repeating them:
    ####if self.plan_generator_active and not self.planning_loop_finished:
      ####if not self.planning_aborted: # if its aborted, ccalc will be in a state that I can use
        ####print "CCALC_INTERACTOR: You seem to be doing things out of order"
        ####import pdb;pdb.set_trace()
    
    ##if not self.planning_loop_finished and not self.planning_aborted:
      
      
    ##self.ccalcfile_name=ccalcfile_name
    ##self.load_com= "loadf \'"+self.ccalcfile_name+"\'.\n"
    ###if self.verbose>0: print "CCALC_INTERACTOR: CCALC LOAD PROBLEM COMMAND:",self.cc_load_com
    ##if self.verbose>0: print "CCALC_INTERACTOR: CCALC LOAD PROBLEM COMMAND:",self.cc_load_com[:-2]
    
    #self.load_and_ground(ccalcfile_name,stats)
    
    #'''If planning is still in progress but was previously aborted, 
    #the planner should keep returning plans in the same iteratore and we unset planning_aborted,
    #effectively renewing the planning.
    #Note that the grounding done above is correct because it was already aborted so should be ready to load and ground. 
    #If the plan generator is not still active then no point running the query - not generator to renew.
    #This all also applies if the planner has simply been exhasted, and this can be learnt by caller by is_planner_exhausted() call'''
    #if self.plan_generator_active and not self.planning_loop_finished: # don't need for it to be aborted, as can just be exhausted and self.planning_aborted:
      #self.planning_aborted=False
      #if self.verbose>0: print "CCALC_INTERACTOR:  --Planning was in progress, restarted with a new file --"
      #if self.verbose>0: print "CCALC_INTERACTOR: SENDING",self.query_com

      #self.ccalc.sendline(self.query_com)
      ##self.ccalc.expect('\?\-')###no no no, the planner getter better take care of that.. otherwise it could get stuck waiting for a nonexistent prompt
      
    #if self.planning_aborted:
      #self.planning_aborted=False
    ##self.planning_loop_finished=False
    #i think this is not supposed to be here self.check_for_plan_continuation()
    

  def check_pred(self,pred):
    predstr=str(pred)#could be a string or a predicate object
    if predstr.strip()[-1]<>'.':predstr=predstr+'.'
    self.pred_cmd=predstr+"\n" #newline because "true" return needs newline to go back to prompt
    self.ccalc.sendline(self.pred_cmd)
    indpred=self.ccalc.expect(['true','false','ERROR: toplevel: Undefined procedure','ERROR: Undefined procedure',pexpect.TIMEOUT])
    if indpred>3:
      import pdb;pdb.set_trace()
    
    
    ind=self.ccalc.expect(['\?\-',pexpect.TIMEOUT])
    if ind>0:
      import pdb;pdb.set_trace()
      
    if indpred==0:
      return True
    if indpred==1:
      return False
    if indpred==2 or indpred==3:
      return None
    import pdb;pdb.set_trace()  
      
  def pred_output(self,pred):
    predstr=str(pred)#could be a string or a predicate object
    if predstr.strip()[-1]<>'.':predstr=predstr.strip()+'.'
    self.pred_cmd=predstr 
    self.ccalc.sendline(self.pred_cmd)
    skippred=0
    while skippred==0:
      skippred=self.ccalc.expect(['\s',predstr])
    #while indped==0:
    indpred=self.ccalc.expect(['true','false','ERROR: toplevel: Undefined procedure','ERROR: Undefined procedure',pexpect.TIMEOUT])
      
    if indpred==0 or indpred==1:
      #outpreds_unparsed=self.ccalc.before.splitlines()
      #outpreds=[ccalc_parser.parse_predicate_list(pred) for pred in outpreds_unparsed]
      outputted=self.ccalc.before.strip()
    else:
      import pdb;pdb.set_trace()
      indpred=""
    #if indpred>3:
      #import pdb;pdb.set_trace()
    
    self.ccalc.sendline('')
    
    ind=self.ccalc.expect(['\?\-',pexpect.TIMEOUT])
    if ind>0:
      import pdb;pdb.set_trace()
      
    #if indpred==0:
      #return True
    #if indpred==1:
      #return False
    #if indpred==2 or indpred==3:
      #return None
    return outputted
    #import pdb;pdb.set_trace()      
      
  def continue_producing_plans(self):
    self.planning_aborted=False
    if self.plan_generator_active: # as long as there is a generator active, we can say it will now be renewed and not self.planning_loop_finished: # don't need for it to be aborted, as can just be exhausted and self.planning_aborted:
      #self.planning_aborted=False
      if self.verbose>0: print "CCALC_INTERACTOR:  --Planning was in progress, restarted with a new file --"
      if self.verbose>0: print "CCALC_INTERACTOR: SENDING",self.query_com

      #self.ccalc.sendline(self.query_com)
      self.ccalc_query()
      #self.ccalc.expect('\?\-')###no no no, the planner getter better take care of that.. otherwise it could get stuck waiting for a nonexistent prompt
    
    #if self.planning_aborted:
      #self.planning_aborted=False
      
    if self.planning_loop_finished:
      self.planning_loop_finished=False
  
  '''The argument, query, will be a query string as is sent to the query.  command of ccalc.'''
  def ccalc_query(self):
    
    if not self.plan_generator_active:
      if self.verbose>0: print "CCALC_INTERACTOR: Generally you will want to run ccalc_query as a part of get_plans - otherwise, how are you going to deal with the output? You may have a way..."
        
    if len(self.query_com)==0 and len(self.query_statements)>0:    
      if self.verbose>1: print "CCALC_INTERACTOR: SENDING query."
      self.ccalc.sendline("query.")
      
      ind = self.ccalc.expect(['enter query \(then ctrl-d\)',pexpect.TIMEOUT])
      if ind<>0: import pdb;pdb.set_trace()
      cntr=0
      for statement in self.query_statements:
        ind = self.ccalc.expect(['\|:',pexpect.TIMEOUT])
        if ind<>0: import pdb;pdb.set_trace()
        
        if statement.strip()[-1]<>'.':
          statement=statement.strip()+'.'
        
        #if cntr%15==14:
        #time.sleep(0.01) # prevent buffer filling? Why is not pexpect taking care of this? no the regex syntax was wrong
        
        if self.verbose>0: print "CCALC_INTERACTOR: SENDING (newlines stripped)",statement
        self.ccalc.sendline(statement)
        
        
      
        
        if ind<>0: import pdb;pdb.set_trace()
      ind=self.ccalc.expect(['\|:',pexpect.TIMEOUT])# prevent buffer filling? 
      if ind<>0: import pdb;pdb.set_trace()
      self.ccalc.sendline("")
      ind=self.ccalc.expect(['\|:',pexpect.TIMEOUT])# prevent buffer filling? 
      if ind<>0: import pdb;pdb.set_trace()
      self.ccalc.sendeof() #off we go
    elif len(self.query_com)>0 and len(self.query_statements)==0:

      if self.verbose>0: print "CCALC_INTERACTOR: SENDING",self.query_com
      if self.verbose>1: print "CCALC_INTERACTOR: **********"
      if self.verbose>1: print "CCALC_INTERACTOR:",self.ccalc.before
      if self.verbose>1: print "CCALC_INTERACTOR: **********"
      if self.verbose>1: print "CCALC_INTERACTOR:",self.ccalc.after
      
      self.ccalc.sendline(self.query_com) #off we go
      
    else:
      print "CCALC_INTERACTOR: Use set_query_label or set_query_string to set up the ccalc query"
      import pdb;pdb.set_trace()
    
    #now we don't wait for any prompt - so this function is different
          
    
  
  def load_pl(self,extfile_name,stats=None,rerun_query=False):
    self.extfile_name=extfile_name
    self.ext_load_com= "[\'"+self.extfile_name+"\'].\n"
    
    if self.plan_generator_active and not self.planning_loop_finished:
      if not self.planning_aborted: # if its aborted, ccalc will be in a state that I can use
        print >> sys.stderr, "You seem to be doing things out of order"
        import pdb;pdb.set_trace()    
    

    if self.verbose>0: print "CCALC_INTERACTOR: SENDING (PL FILE)",self.ext_load_com
    self.ccalc.sendline(self.ext_load_com)
    
    
    if self.verbose>1: print "CCALC_INTERACTOR: **********"
    if self.verbose>1: print "CCALC_INTERACTOR:",self.ccalc.before
    if self.verbose>1: print "CCALC_INTERACTOR: **********"
    if self.verbose>1: print "CCALC_INTERACTOR:",self.ccalc.after
    
    
    #self.loading_string=""
    
    self.bad_preds=[]
    ind=1
    self.bad_file_error=None
    
    while ind<>0:
      ind=self.ccalc.expect(['\?\-',
                             'Goal \(directive\) failed: user:.*\(.*\)',
                             'Error: .* is not an atomicFormula'])

      if ind==0:
        
        def pl_compile_time(visitor,goutput):
          rexp=".*% .+ext.+.pl compiled (.+) sec, .+ bytes.*"
          res=re.search(rexp,goutput,re.MULTILINE)
          if res is None:
            print >>sys.stderr, "CCALC_INTERACTOR: Possible failed load. ccalc output:\n",self.ccalc.before,"\n",self.ccalc.after
            import pdb;pdb.set_trace()
          if len(res.groups())<1:
            print >>sys.stderr,"CCALC_INTERACTOR: CCalcInteractor.loading_time() must be called right after load_and_ground()"
            import pdb;pdb.set_trace()
          visitor.extern_compile_time=float(res.group(1))      
          
        if stats is not None:
          pl_compile_time(stats,self.ccalc.before+self.ccalc.after)
          
          
      if ind==1:

        preds=ccalc_parser.parse_predicate_list(self.ccalc.after[25:])
        self.bad_preds=self.bad_preds+preds
        if self.verbose>0: 
          print >>sys.stderr,"Bad pred:",preds
      if ind==2:
        self.bad_file_error=self.ccalc.after
        if self.verbose>0: 
          print >>sys.stderr,"Failed to ground:",self.bad_file_error
      
    #self.loading_string=self.loading_string+self.ccalc.before
    if rerun_query:
      self.continue_producing_plans()
  
  def load_and_ground(self,ccalcfile_name,stats=None,rerun_query=False):
    
    if self.plan_generator_active and not self.planning_loop_finished:
      if not self.planning_aborted: # if its aborted, ccalc will be in a state that I can use
        print >> sys.stderr, "You seem to be doing things out of order"
        import pdb;pdb.set_trace()
    
    self.ccalcfile_name=ccalcfile_name
    #self.cc_load_com= "loadf \'"+self.ccalcfile_name+"\'.\n"
    self.cc_load_com= "loadf \'"+self.ccalcfile_name+"\'."
    #if self.verbose>0: print "CCALC_INTERACTOR: CCALC LOAD PROBLEM COMMAND:",self.cc_load_com[:-2]
    
    if self.verbose>0: print "CCALC_INTERACTOR: SENDING (CCALC FILE)",self.cc_load_com
    self.ccalc.sendline(self.cc_load_com)
    
    
    if self.verbose>1: print "CCALC_INTERACTOR: **********"
    if self.verbose>1: print "CCALC_INTERACTOR:",self.ccalc.before
    if self.verbose>1: print "CCALC_INTERACTOR: **********"
    if self.verbose>1: print "CCALC_INTERACTOR:",self.ccalc.after
        
    
    
    
    #self.loading_string=""
    
    self.bad_preds=[]
    ind=1
    self.bad_file_error=None
    
    while ind<>0:
      #ind=self.ccalc.expect(['\?\-',
      ind=self.ccalc.expect(['true ',
                             'Goal \(directive\) failed: user:.*\(.*\)',
                             'Error: .* is not an atomicFormula',
                             'ERROR: .*: Syntax error: ',
                             pexpect.TIMEOUT])
      #self.loading_string=self.loading_string+self.ccalc.before
      #self.loading_string=self.loading_string+self.ccalc.after
      
      if ind==0:
        
        def ccalc_grounding_time_parse(visitor,goutput):
          rexp="^% Grounding time: ([0-9]+\.[0-9]+) seconds"
          res=re.search(rexp,goutput,re.MULTILINE)
          visitor.ccalc_grounding_time=float(res.group(1))

        def ccalc_completion_time_parse(visitor,goutput):  
          rexp="^% Completion time: ([0-9]+\.[0-9]+) seconds"
          res=re.search(rexp,goutput,re.MULTILINE)
          visitor.ccalc_completion_time=float(res.group(1))     

          
        def ccalc_total_time_parse(visitor,goutput):
          rexp="^% Total time: ([0-9]+\.[0-9]+) seconds"
          res=re.search(rexp,goutput,re.MULTILINE)
          visitor.ccalc_loading_time=float(res.group(1))        
                      
        if stats is not None:
          ccalc_grounding_time_parse(stats,self.ccalc.before+self.ccalc.after)
          ccalc_completion_time_parse(stats,self.ccalc.before+self.ccalc.after)
          ccalc_total_time_parse(stats,self.ccalc.before+self.ccalc.after)
          
      if ind==1:

        preds=ccalc_parser.parse_predicate_list(self.ccalc.after[25:])
        self.bad_preds=self.bad_preds+preds
        if self.verbose>0: 
          print >>sys.stderr,"CCALC_INTERACTOR: Bad pred:",preds
      if ind==2:
        self.bad_file_error=self.ccalc.after
        if self.verbose>0: 
          print >>sys.stderr,"CCALC_INTERACTOR: Failed to ground:",self.bad_file_error
      if ind==3:
        self.syntax_error=self.ccalc.after
        if self.verbose>0: 
          print >>sys.stderr,"CCALC_INTERACTOR: Syntax error:",self.syntax_error        
        import pdb;pdb.set_trace()          
      if ind==4:
        import pdb;pdb.set_trace()
      
    
    
    if self.verbose>0: print "CCALC_INTERACTOR: SENDING EOL"
    
    
    
    if self.verbose>1: print "CCALC_INTERACTOR: **********"
    if self.verbose>1: print "CCALC_INTERACTOR:",self.ccalc.before
    if self.verbose>1: print "CCALC_INTERACTOR: **********"
    if self.verbose>1: print "CCALC_INTERACTOR:",self.ccalc.after
        
    self.ccalc.sendline("")
    
    self.grounding_done=True
    #self.loading_string=self.loading_string+self.ccalc.before
    ind=self.ccalc.expect(['\?\-',pexpect.TIMEOUT])
    if ind<>0:import pdb;pdb.set_trace()
    
    
    
    if self.verbose>1: print "CCALC_INTERACTOR: **********"
    if self.verbose>1: print "CCALC_INTERACTOR:",self.ccalc.before
    if self.verbose>1: print "CCALC_INTERACTOR: **********"
    if self.verbose>1: print "CCALC_INTERACTOR:",self.ccalc.after
        
    if rerun_query:
      self.continue_producing_plans()
    
    
    #self.planning_loop_finished=False

  ''' A class to keep track of some stats.'''
  #class CCalcTimings:
    #def __init__(self):
      #pass
      #self.pl_compile_time=0
      #self.ccalc_grounding_time=0
      #self.ccalc_completion_time=0
      #self.ccalc_total_time=0
      

        
      
      #self.ccalc_loading_time=0 
  
  '''Call this after loading to get some stats back, parsed from CCalc.'''
  #def loading_time(self):

    #loading_obj=self.CCalcLoadingTime()
    #self.loading_time_parse(loading_obj,self.loading_string)
    #self.loading_string=""
        
    #if self.verbose>1: print "CCALC_INTERACTOR: **********"
    #if self.verbose>1: print "CCALC_INTERACTOR:",self.ccalc.before
    #if self.verbose>1: print "CCALC_INTERACTOR: **********"
    #if self.verbose>1: print "CCALC_INTERACTOR:",self.ccalc.after
    #return loading_obj
  
  '''Utility function'''
    
  


  def abort_planning(self):
    if not self.planning_loop_finished:
      self.ccalc.sendintr()
      self.ccalc.sendintr()
      
      '''The problem with sending two interrupts in quick succession followed by an abort character "a" is that the interrupt is processed asynchronously.
      This means that sometimes the order is actually <intrpt> a <intrpt> rather than <intrpt> <intrpt> a.
      One solution would be to introduce a wait, but better to not invite in race conditions.'''
      '''The solution here is just to wait for the interrupt prompt to come up.'''
      #self.ccalc.sendintr() #we send two interrupts because if planning is already finished we want to get the Action prompt.

      if self.verbose>0: print "CCALC_INTERACTOR: Sending interrupt to CCalc."
      #self.ccalc.expect('Action \(h for help\) \? ')  
      ind=self.ccalc.expect(['Action \(h for help\) \? ',pexpect.TIMEOUT])  
      #ind=self.ccalc.expect(['\?\-','Action \(h for help\) \? ',pexpect.TIMEOUT])  

      #if ind==0:
        #if self.verbose>1: print "CCALC_INTERACTOR: Already at prompt. Interrupt ignored."
      if ind==0:
        if self.verbose>1: print "CCALC_INTERACTOR: Interrupt caught. Aborting and waiting for prompt"
        #if self.verbose>1: print "CCALC_INTERACTOR: Interrupt caught. Aborting." # prompt might be useful for get_plans
        self.ccalc.send('a') #no newline
        '''we couldintroduce a time wait here to stop from sending before the abort is dealt with.
        - But I choose to wait on the prompt and then get request a new one for get-plans.'''
        
        ind=self.ccalc.expect(['\?\-','Action \(h for help\) \? ',pexpect.TIMEOUT])  
        if ind==0:
          #self.ccalc.sendline('true.')#should get us a prompt, without further difficulty..
          pass # well, we have a prompt already
        elif ind==1:
          '''This can happen if the second interrupted interrupted the first interrupt, or something like that...'''
          self.ccalc.send('a') #no newline
          ind2=self.ccalc.expect(['\?\-',pexpect.TIMEOUT])  
          if ind2==0:
            pass#expected
          else:import pdb;pdb.set_trace()
        else:import pdb;pdb.set_trace()
        
        
        
      else:
        import pdb;pdb.set_trace()
      
      #self.ccalc.send('\n') #Don't do this. Would cause it to stall on an unexpected prompt.
    else:
      if self.verbose>0: print "CCALC_INTERACTOR: No point aborting - planning already finished"
     # if planning is out of loop, it has already waited on this self.ccalc.expect('\?\-')  
    self.planning_aborted=True
    
  '''This returns a generator'''
  def get_plans(self,stats=None):
    
    self.plan_generator_active=True
    self.planning_loop_finished=False
    
    self.planning_aborted=False
    if not self.grounding_done:
      print >> sys.stderr, "CCALC_INTERACTOR: Do the grounding before trying to pull out any plans"
      import pdb;pdb.set_trace()
    
    if len(self.bad_preds)>0:
      if self.verbose>0: 
        print >>sys.stderr,"CCALC_INTERACTOR: Bad preds found; no plans will be obtained."
      import pdb;pdb.set_trace()
      self.planning_loop_finished=True
      
    
    if self.bad_file_error is not None:
      if self.verbose>0:
        print >>sys.stderr,"CCALC_INTERACTOR: Bad file - no plans will be obtained:",self.bad_file_error
      import pdb;pdb.set_trace()
      self.planning_loop_finished=True
      
    
    #if self.verbose>0: print "CCALC_INTERACTOR: SENDING",self.query_com
    #if self.verbose>1: print "CCALC_INTERACTOR: **********"
    #if self.verbose>1: print "CCALC_INTERACTOR:",self.ccalc.before
    #if self.verbose>1: print "CCALC_INTERACTOR: **********"
    #if self.verbose>1: print "CCALC_INTERACTOR:",self.ccalc.after
    
    #self.ccalc.sendline(self.query_com)
    self.ccalc_query()

    first_solution_started=False
    planfound=False

    while not self.planning_loop_finished:
      if self.planning_aborted:
        self.planning_loop_finished=True
        break
      ind=self.ccalc.expect(['true ',
      #ind=self.ccalc.expect(['true \.',
                            'Solution time: [\.0-9]* seconds \(prep [\.0-9]* seconds, search [\.0-9]* seconds\)',
                            'No solution with maxstep [0-9]+\.',
                            'Solution [0-9]+:',
                            'ERROR: Not enough resources',
                            'Arguments are not sufficiently instantiated',
                            "Type error: .atom. expected, found .pipe\(uname\).",
                            'Exception: \(',
                            'Resources exceeded... search aborted.',
                            'Error: .* is not an atomicFormula',
                            'is not an atomicFormula',
                            'ERROR: Out of global stack',
                            pexpect.TIMEOUT])  
                            
      if ind==0:      
        if self.verbose>0: print "CCALC_INTERACTOR: PLANNING FINISHED."
        if self.verbose>1: print "CCALC_INTERACTOR: **********"
        if self.verbose>1: print "CCALC_INTERACTOR:",self.ccalc.before
        if self.verbose>1: print "CCALC_INTERACTOR: **********"
        if self.verbose>1: print "CCALC_INTERACTOR:",self.ccalc.after      
        self.planning_loop_finished=True
        #if not self.planning_aborted:
        rawplan=self.ccalc.before
        self.ccalc.sendline("")
        self.ccalc.expect(['\?\-',pexpect.TIMEOUT])
        if ind<>0:import pdb;pdb.set_trace()
        if self.verbose>1: print "CCALC_INTERACTOR: **********"
        if self.verbose>1: print "CCALC_INTERACTOR:",self.ccalc.before
        if self.verbose>1: print "CCALC_INTERACTOR: **********"
        if self.verbose>1: print "CCALC_INTERACTOR:",self.ccalc.after              
        if len(rawplan)>20:
          if self.verbose>1: print "CCALC_INTERACTOR: YIELDING LAST PLAN."
          yield rawplan
          planfound=True
          
      elif ind==1 or ind==2:
        if ind==1:
          def add_ccalc_grounding_time_parse(visitor,goutput):
            rexp="^% Solution time: ([0-9]+\.*[0-9]*) seconds"
            res=re.search(rexp,goutput,re.MULTILINE)
            visitor.ccalc_solution_time=visitor.ccalc_solution_time+float(res.group(1))          
          if stats is not None:
            add_ccalc_grounding_time_parse(stats,self.ccalc.before+self.ccalc.after)
          #import pdb;pdb.set_trace()
        if self.verbose>1: print "CCALC_INTERACTOR: ...passing guff...."
        if self.verbose>1: print "CCALC_INTERACTOR: **********"
        if self.verbose>1: print "CCALC_INTERACTOR:",self.ccalc.before
        if self.verbose>1: print "CCALC_INTERACTOR: **********"
        if self.verbose>1: print "CCALC_INTERACTOR:",self.ccalc.after      
        
      
      elif ind==3:
        if self.verbose>1: print "CCALC_INTERACTOR: ...ccalc says solution...."
        if self.verbose>1: print "CCALC_INTERACTOR: **********"
        if self.verbose>1: print "CCALC_INTERACTOR:",self.ccalc.before
        if self.verbose>1: print "CCALC_INTERACTOR: **********"
        if self.verbose>1: print "CCALC_INTERACTOR:",self.ccalc.after      
        if first_solution_started:
          if len(self.ccalc.before)>20:
            if self.verbose>1: print "CCALC_INTERACTOR: YIELDING A PLAN (not last one, of a multi-plan thingy)."
            yield self.ccalc.before          
            planfound=True
        else:
          if self.verbose>1: print "CCALC_INTERACTOR: ...awaiting first solution..."
          first_solution_started=True
      elif ind==4 or ind==5 or ind==6 or ind==7 or ind==8 or ind==9 or ind==10:
        if self.verbose>0: print "CCALC_INTERACTOR: ERROR:"
        if self.verbose>0: print "CCALC_INTERACTOR: **********"
        if self.verbose>0: print "CCALC_INTERACTOR:",self.ccalc.before
        if self.verbose>0: print "CCALC_INTERACTOR: **********"
        if self.verbose>0: print "CCALC_INTERACTOR:",self.ccalc.after 
        #import pdb;pdb.set_trace()
        self.planning_loop_finished=True
        self.ccalc.send('a')
        self.ccalc.send('\n')
        ind2=self.ccalc.expect(['\?\-',pexpect.TIMEOUT])  
        if ind2>0:
          import pdb;pdb.set_trace()
      elif ind==11:
        # returns us to the prompt anyway
        if self.verbose>0: print "CCALC_INTERACTOR: ERROR:"
        if self.verbose>0: print "CCALC_INTERACTOR: **********"
        if self.verbose>0: print "CCALC_INTERACTOR:",self.ccalc.before
        if self.verbose>0: print "CCALC_INTERACTOR: **********"
        if self.verbose>0: print "CCALC_INTERACTOR:",self.ccalc.after 
        #import pdb;pdb.set_trace()
        self.planning_loop_finished=True
        ind2=self.ccalc.expect(['\?\-',pexpect.TIMEOUT])  
        if ind2>0:
          import pdb;pdb.set_trace()        
      #elif ind==5:
        #if self.verbose>0: print "CCALC_INTERACTOR: ERROR: 'Arguments not sufficiently instansiated'..."
        #if self.verbose>1: print "CCALC_INTERACTOR: **********"
        #if self.verbose>1: print "CCALC_INTERACTOR:",self.ccalc.before
        #if self.verbose>1: print "CCALC_INTERACTOR: **********"
        #if self.verbose>1: print "CCALC_INTERACTOR:",self.ccalc.after 
        #import pdb;pdb.set_trace()
        #self.planning_loop_finished=True
        #self.ccalc.send('a')
        #self.ccalc.send('\n')
        #ind2=self.ccalc.expect(['\?\-',pexpect.TIMEOUT])  
        #if ind2>0:
          #import pdb;pdb.set_trace()
      #elif ind==5:
        #if self.verbose>0: print "CCALC_INTERACTOR: ERROR: 'Arguments not sufficiently instansiated'..."
        #if self.verbose>1: print "CCALC_INTERACTOR: **********"
        #if self.verbose>1: print "CCALC_INTERACTOR:",self.ccalc.before
        #if self.verbose>1: print "CCALC_INTERACTOR: **********"
        #if self.verbose>1: print "CCALC_INTERACTOR:",self.ccalc.after 
        #import pdb;pdb.set_trace()
        #self.planning_loop_finished=True
        #self.ccalc.send('a')
        #self.ccalc.send('\n')
        #ind2=self.ccalc.expect(['\?\-',pexpect.TIMEOUT])  
        #if ind2>0:
          #import pdb;pdb.set_trace()          

      else:
        import pdb;pdb.set_trace()
        if self.verbose>0: print "CCALC_INTERACTOR: TIMEOUT"
        print >> sys.stderr,"CCALC_INTERACTOR: TIMEOUT"      
        self.planning_loop_finished=True

    if self.verbose>0: print "CCALC_INTERACTOR: PLAN FOUND",planfound
    
    self.plan_generator_active=False
    if self.verbose>0: print "CCALC_INTERACTOR: Planning fin"
    
  def close(self):

    if self.plan_generator_active and not self.planning_loop_finished:
      if not self.planning_aborted:
        #print "CCALC_INTERACTOR: Please exhaust all plans before closing (or change this code to allow early cancellation) - use abort_planning"
        print "you seem to be doing things out of order"
        import pdb;pdb.set_trace()
    self.ccalc.sendline('halt.')
    
    self.ccalc.expect(pexpect.EOF)
    
    self.ccalc.close()      
        