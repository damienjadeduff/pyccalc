#!/usr/bin/env python

'''     Example file for pyccalc usage (a simpler example).
        
        --------------------------------------------------------------------
        
        Copyright (C) 2013  Damien Jade Duff (Delfina's Dad)

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <http://www.gnu.org/licenses/>.
            
'''   
import ccalc_parser
import ccalc_interactor

'''Setup CCalc'''
ccalc_interactor.swipl_loc="swipl -G1g -T1g -L1g" # give me lots of memory! 
ccalc_interactor.ccalc_loc="/opt/software/ccalc/ccalc.pl"
ccalc_interactor.default_solver="grasp"

'''Instantiate an interpreter'''
ccalc=ccalc_interactor.CCalcInteractor(logfile_name="simple_ccalc_test_2_log.txt",num_to_ask=10,verbose=1)
  
'''Load the domain'''
ccalc.load_and_ground("example_files/bw_2_countvar.ccalc")

'''Setup the query'''
query=[]
query.append("maxstep :: 0..5.")
query.append("0: is_below(nut1,bolt).")
query.append("maxstep: orientation_is(nut1)=vert,is_at(nut1)=bolt,orientation_is(nut2)=vert,is_at(nut2)=nut1,orientation_is(bolt)=horiz_x,is_at(bolt)=loc1.")
query.append("T=<maxstep ->> (T: orientation_is(nut1)=vert,orientation_is(bolt)=horiz_x).")


ccalc.set_query_statements(query)

'''Create the generator which will generate the plans'''
plan_generator=ccalc.get_plans()
cntr=0

'''Loop through the plans.'''
for raw_plan in plan_generator:
    '''Parse the plan into a list of states and actions'''
    plan=ccalc_parser.parse_plans(raw_plan)[0]
    '''Print out the plan length and the plan itself'''
    print "Plan #",cntr,"Length",len(plan.actions),"\n",plan
    '''Show that we can quit halfway through'''
    if cntr>20:
      ccalc.abort_planning()
    cntr=cntr+1
    
print "Finished after",cntr,"plans."